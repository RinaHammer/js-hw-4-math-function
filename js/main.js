/*Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера два числа.
Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
Створити функцію, в яку передати два значення та операцію.
Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).*/

function performMathOperation() {
  let number1 = prompt("Введіть перше число:");
  let number2 = prompt("Введіть друге число:");
  let operation = prompt("Введіть математичну операцію (+, -, *, /):");

  // Перевірка коректності введених значень
  if (!isValidNumber(number1) || !isValidNumber(number2)) {
    alert("Будь ласка, введіть числа!");
    performMathOperation(); // Запитати числа знову
    return;
  }

  number1 = parseFloat(number1.replace(",", "."));
  number2 = parseFloat(number2.replace(",", "."));

  calculateResult(number1, number2, operation);
}

function calculateResult(number1, number2, operation) {
  let result;
  switch (operation) {
    case "+":
      result = number1 + number2;
      break;
    case "-":
      result = number1 - number2;
      break;
    case "*":
      result = number1 * number2;
      break;
    case "/":
      result = number1 / number2;
      break;
    default:
      alert("Будь ласка, введіть математичну операцію (+, -, *, /):");
      performMathOperation(); // Запитати операцію знову
      return;
  }

  console.log(`Результат: ${result}`);
}

function isValidNumber(value) {
  // Перевірка, чи є значення числом
  return /^-?\d*(\.\d+)?$/.test(value.replace(",", "."));
}

// Виклик функції
performMathOperation();

// Стара версія робочого коду, але з поганою логікою, бо функція викликає сама себе

// function performMathOperation() {
//   let number1 = prompt("Введіть перше число:");
//   let number2 = prompt("Введіть друге число:");
//   let operation = prompt("Введіть математичну операцію (+, -, *, /):");

//   // Перевірка коректності введених значень
//   if (!isValidNumber(number1) || !isValidNumber(number2)) {
//     alert("Будь ласка, введіть числа!");
//     performMathOperation(); // Запитати числа знову
//     return;
//   }

//   number1 = parseFloat(number1.replace(",", "."));
//   number2 = parseFloat(number2.replace(",", "."));

//   let result;
//   switch (operation) {
//     case "+":
//       result = number1 + number2;
//       break;
//     case "-":
//       result = number1 - number2;
//       break;
//     case "*":
//       result = number1 * number2;
//       break;
//     case "/":
//       result = number1 / number2;
//       break;
//     default:
//       alert("Будь ласка, введіть математичну операцію (+, -, *, /):");
//       performMathOperation(); // Запитати операцію знову
//       return;
//   }

//   console.log(`Результат: ${result}`);
// }

// function isValidNumber(value) {
//   // Перевірка, чи є значення числом
//   return /^-?\d*(\.\d+)?$/.test(value.replace(",", "."));
// }
